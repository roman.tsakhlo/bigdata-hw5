const { tables } = require("./tableNames");

const queries = {
  queryInsertFeedbackByProductId: `
  INSERT INTO ${tables.feedback_by_product_id} (
        product_id, review_id, star_rating, customer_id, review_headline, review_body, review_date
    ) VALUES (?, ?, ?, ?, ?, ?, ?);
    `,
  queryInsertFeedbackByProductIdAndStarRating: `
  INSERT INTO ${tables.feedback_by_product_id_and_star_rating} (
        product_id, star_rating, review_id, review_headline, review_body, review_date
    ) VALUES (?, ?, ?, ?, ?, ?);
    `,
  queryInsertFeedbackByByCustomerId: `
  INSERT INTO ${tables.feedback_by_customer_id} (
    customer_id, star_rating, product_id, review_id, review_headline, review_body, review_date
    ) VALUES (?, ?, ?, ?, ?, ?, ?);
`,
  queryInsertCountReviewsProductId: `
  INSERT INTO ${tables.product_id_review_count} (
    review_count, review_date, product_id
    ) VALUES (?, ?, ?);
`,
  queryInsertCountReviewsCustomerId: `
  INSERT INTO ${tables.customer_id_review_count} (
    review_count, review_date, customer_id
    ) VALUES (?, ?, ?);
`,
  queryInsertCountReviewsCustomerIdBackers: `
  INSERT INTO ${tables.customer_id_backers} (
    c_backers, review_date, customer_id
    ) VALUES (?, ?, ?);
`,
  queryInsertCountCustomerIdHaters: `
  INSERT INTO ${tables.customer_id_haters} (
    c_haters, review_date, customer_id
    ) VALUES (?, ?, ?);
`,
};

module.exports.insert = (client, values, query) => {
  client.execute(queries[query], values, (err, result) => {
    if (err) {
      console.error(`ERROR ${query} QUERY:`, err);
    } else {
      console.log(`${query} - Data inserted successfully`, result);
    }
  });
};
