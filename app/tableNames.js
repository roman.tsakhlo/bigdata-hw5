module.exports.tables = {
  feedback_by_product_id: "feedback_by_product_id",
  feedback_by_product_id_and_star_rating:
    "feedback_by_product_id_and_star_rating",
  feedback_by_customer_id: "feedback_by_customer_id",
  product_id_review_count: "product_id_review_count",
  customer_id_review_count: "customer_id_review_count",
  customer_id_backers: "customer_id_backers",
  customer_id_haters: "customer_id_haters",
};
