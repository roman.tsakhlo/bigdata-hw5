const cassandra = require("cassandra-driver");
const express = require("express");
const { Client } = require("cassandra-driver");
const fs = require("fs");
const path = require("path");

const { dateFormat } = require("./dateFormat");
const { tables } = require("./tableNames");
const {
  queryTableFeedbackByProductId,
  queryTableFeedbackByProductIdAndStarRating,
  queryTableFeedbackByCustomerId,
  queryTableCountReviewsProductId,
  queryTableCountReviewsCustomerId,
  queryTableCountReviewsCustomerIdBackers,
  queryTableCountCustomerIdHaters,
} = require("./tables");
const { insert } = require("./insert");

const keyspaceName = "hw5";

const client = new cassandra.Client({
  contactPoints: ["127.0.0.1"],
  localDataCenter: "datacenter1",
  keyspace: keyspaceName,
  // keyspace: "system",
});

runCassandra();
async function runCassandra() {
  try {
    await client.connect();
    // await createKeySpace();
    // await createTables();
    // await insertData();
    console.log("INSIDE runCassandra");
  } catch (error) {
    console.error("RUN ERROR", error);
  }
}

async function createKeySpace() {
  const queryKeyspace = `
  CREATE KEYSPACE IF NOT EXISTS ${keyspaceName}
  WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3'};
  `;

  await client.execute(queryKeyspace, (err) => {
    if (err) {
      console.error("ERROR CREATING KEYSPACE:", err);
    } else {
      console.log(`Keyspace '${keyspaceName}' created or already exists`);
    }
  });
}

async function createTables() {
  await client.execute(queryTableFeedbackByProductId);
  await client.execute(queryTableFeedbackByProductIdAndStarRating);
  await client.execute(queryTableFeedbackByCustomerId);
  await client.execute(queryTableCountReviewsProductId);
  await client.execute(queryTableCountReviewsCustomerId);
  await client.execute(queryTableCountReviewsCustomerIdBackers);
  await client.execute(queryTableCountCustomerIdHaters);
  console.log("Table created successfully");
}

async function insertData() {
  const pathToFile = path.join(__dirname, "amazon_reviews.csv");
  const fileData = fs.readFileSync(pathToFile, "utf-8");
  const lines = fileData.split("\n");
  const headers = lines[0].split(",");

  for (let i = 1; i < lines.length / 1000; i++) {
    const allValues = lines[i].split(",");
    const fields = {};

    for (let j = 0; j < headers.length; j++) {
      fields[headers[j]] = allValues[j];
    }

    if (String(new Date(fields.review_date)) !== "Invalid Date") {
      let allReviewForProductId = {};
      let allReviewForCustomerId = {};
      let allCustomersBackers = {};
      let allCustomersHaters = {};

      if (!Array.isArray(allReviewForProductId[fields.product_id])) {
        allReviewForProductId[fields.product_id] = [];
      }
      if (!Array.isArray(allReviewForCustomerId[fields.product_id])) {
        allReviewForCustomerId[fields.customer_id] = [];
      }

      // console.log(fields.star_rating);
      // console.log(typeof fields.star_rating);

      if (fields.star_rating === "4" || fields.star_rating === "5") {
        if (!allCustomersBackers[fields.customer_id]) {
          allCustomersBackers[fields.customer_id] = 0;
        } else {
          allCustomersBackers[fields.customer_id]++;
        }
      }

      if (fields.star_rating === "1" || fields.star_rating === "2") {
        if (!allCustomersHaters[fields.customer_id]) {
          allCustomersHaters[fields.customer_id] = 0;
        } else {
          allCustomersHaters[fields.customer_id]++;
        }
      }

      allReviewForProductId[fields.product_id].push(fields.review_id);
      allReviewForCustomerId[fields.customer_id].push(fields.review_id);

      let valuesProductId = [
        fields.product_id,
        fields.review_id,
        fields.star_rating,
        fields.customer_id,
        fields.review_headline,
        fields.review_body,
        fields.review_date,
      ];

      let valuesProductIdAndStarRating = [
        fields.product_id,
        fields.star_rating,
        fields.review_id,
        fields.review_headline,
        fields.review_body,
        fields.review_date,
      ];

      let valuesCustomerId = [
        fields.customer_id,
        fields.star_rating,
        fields.product_id,
        fields.review_id,
        fields.review_headline,
        fields.review_body,
        fields.review_date,
      ];

      let valuesProductIdReviewCount = [
        String(allReviewForProductId[fields.product_id].length),
        fields.review_date,
        fields.product_id,
      ];

      let valuesCustomerIdReviewCount = [
        String(allReviewForCustomerId[fields.customer_id].length),
        fields.review_date,
        fields.customer_id,
      ];

      // 'cb' and 'ch' can be all "0", can be mistakes on allCustomersBackers, allCustomersHaters
      let cb = allCustomersBackers[fields.customer_id]
        ? String(allCustomersBackers[fields.customer_id])
        : "0";
      let ch = allCustomersHaters[fields.customer_id]
        ? String(allCustomersHaters[fields.customer_id])
        : "0";

      let valuesCustomersBackers = [cb, fields.review_date, fields.customer_id];

      let valuesCustomersHaters = [ch, fields.review_date, fields.customer_id];

      insert(client, valuesProductId, "queryInsertFeedbackByProductId");
      insert(
        client,
        valuesProductIdAndStarRating,
        "queryInsertFeedbackByProductIdAndStarRating"
      );
      insert(client, valuesCustomerId, "queryInsertFeedbackByByCustomerId");
      insert(
        client,
        valuesProductIdReviewCount,
        "queryInsertCountReviewsProductId"
      );
      insert(
        client,
        valuesCustomerIdReviewCount,
        "queryInsertCountReviewsCustomerId"
      );
      insert(
        client,
        valuesCustomersBackers,
        "queryInsertCountReviewsCustomerIdBackers"
      );
      insert(client, valuesCustomersHaters, "queryInsertCountCustomerIdHaters");
    }
  }

  console.log("Data loaded successfully");
}

// -----------------------------

// 9000
const PORT = process.env.PORT || 8080;
const app = express();
app.use(express.json());

//
app.get("/api/hw", async (req, res) => {
  let result = {};
  const {
    product_id,
    star_rating,
    customer_id,
    review_count,
    customer_count,
    review_date,
    c_backers,
    c_haters,
  } = req.query;

  if (product_id && !star_rating) {
    const result = await client.execute(
      `SELECT * FROM ${tables.feedback_by_product_id} WHERE product_id = ${product_id}`
    );
    res.json(result.rows);
    return;
  } else if (product_id && star_rating) {
    const result = await client.execute(
      `SELECT * FROM ${tables.feedback_by_product_id_and_star_rating} WHERE product_id = ${product_id} AND star_rating = ${star_rating};`
    );
    res.json(result.rows);
    return;
  } else if (customer_id) {
    const result = await client.execute(
      `SELECT * FROM ${tables.feedback_by_customer_id} WHERE customer_id = ${customer_id};`
    );
    res.json(result.rows);
    return;
  } else if (review_count && review_date) {
    const result = await client.execute(
      `SELECT product_id FROM ${tables.product_id_review_count} WHERE review_count = ${review_count} AND review_date = ${review_date};`
    );
    res.json(result.rows);
    return;
  } else if (customer_count && review_date) {
    const result = await client.execute(
      `SELECT customer_id FROM ${tables.customer_id_review_count} WHERE review_count = ${customer_count} AND review_date = ${review_date};`
    );
    res.json(result.rows);
    return;
  } else if (c_backers && review_date) {
    const result = await client.execute(
      `SELECT customer_id FROM ${tables.customer_id_backers} WHERE c_backers = ${c_backers} AND review_date = ${review_date};`
    );
    res.json(result.rows);
    return;
  } else if (c_haters && review_date) {
    const result = await client.execute(
      `SELECT customer_id FROM ${tables.customer_id_haters} WHERE c_haters = ${c_haters} AND review_date = ${review_date};`
    );
    res.json(result.rows);
    return;
  } else {
    result = await client.execute(
      `SELECT * FROM ${tables.customer_id_backers}`
    );
    res.json(result.rows);
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
