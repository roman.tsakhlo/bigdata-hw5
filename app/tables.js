const { tables } = require("./tableNames");

module.exports.queryTableFeedbackByProductId = `
CREATE TABLE IF NOT EXISTS ${tables.feedback_by_product_id} (
    product_id TEXT,
    review_id TEXT,
    star_rating TEXT,
    customer_id TEXT,
    review_headline TEXT,
    review_body TEXT,
    review_date TEXT,
    PRIMARY KEY (product_id, review_id)
);
`;

module.exports.queryTableFeedbackByProductIdAndStarRating = `
CREATE TABLE IF NOT EXISTS ${tables.feedback_by_product_id_and_star_rating} (
    product_id TEXT,
    star_rating TEXT,
    review_id TEXT,
    review_headline TEXT,
    review_body TEXT,
    review_date TEXT,
    PRIMARY KEY ((product_id, star_rating))
);
`;

module.exports.queryTableFeedbackByCustomerId = `
CREATE TABLE IF NOT EXISTS ${tables.feedback_by_customer_id} (
    customer_id TEXT,
    star_rating TEXT,
    product_id TEXT,
    review_id TEXT,
    review_headline TEXT,
    review_body TEXT,
    review_date TEXT,
    PRIMARY KEY(customer_id, star_rating)
);
`;

module.exports.queryTableCountReviewsProductId = `
CREATE TABLE IF NOT EXISTS ${tables.product_id_review_count} (
    review_count TEXT,
    review_date TEXT,
    product_id TEXT,
    PRIMARY KEY ((review_count, review_date))
);
`;

module.exports.queryTableCountReviewsCustomerId = `
CREATE TABLE IF NOT EXISTS ${tables.customer_id_review_count} (
    review_count TEXT,
    review_date TEXT,
    customer_id TEXT,
    PRIMARY KEY ((review_count, review_date))
);
`;
module.exports.queryTableCountReviewsCustomerIdBackers = `
CREATE TABLE IF NOT EXISTS ${tables.customer_id_backers} (
    c_backers TEXT,
    review_date TEXT,
    customer_id TEXT,
    PRIMARY KEY ((c_backers, review_date))
);
`;

module.exports.queryTableCountCustomerIdHaters = `
CREATE TABLE IF NOT EXISTS ${tables.customer_id_haters} (
    c_haters TEXT,
    review_date TEXT,
    customer_id TEXT,
    PRIMARY KEY ((c_haters, review_date))
);
`;
